//
//  Colors.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 19/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import UIKit

class Theme{
    var navigationBarBackgroundColor : UIColor!
    var navigationBarTintColor : UIColor!
    var layoutBackgroundColor : UIColor!
    var cellBackgroundColor : UIColor!
    
    static func black() -> Theme{
        let black = Theme()
        black.navigationBarBackgroundColor = UIColor.generateColor(23.0, green: 29.0, blue: 32.0, alpha: 1)
        //black.navigationBarTintColor = UIColor.generateColor(171.0, green: 248.0, blue: 189.0, alpha: 1)
        black.navigationBarTintColor = UIColor.generateColor(255.0, green: 94.0, blue: 0, alpha: 1) // try orange
        black.layoutBackgroundColor = UIColor(red: 0.196078, green: 0.266667, blue: 0.305882, alpha: 1)
        black.cellBackgroundColor = UIColor.black
        return black
    }
    
    static func orange() -> Theme{
        let orange = Theme()
        orange.navigationBarBackgroundColor = UIColor.generateColor(255.0, green: 94.0, blue: 0, alpha: 1)
        orange.navigationBarTintColor = UIColor.generateColor(245, green: 245, blue: 242, alpha: 1)
        orange.layoutBackgroundColor = UIColor.generateColor(245, green: 245, blue: 242, alpha: 1)
        orange.cellBackgroundColor = UIColor.lightGray
        return orange
    }
}

let theme = Theme.black()

extension UIColor {
    
    static func navigationBarBackgroundColor() -> UIColor {
        return theme.navigationBarBackgroundColor
    }
    
    static func navigationBarTintColor() -> UIColor {
        return theme.navigationBarTintColor
    }
        
    
    static func layoutBackgroundColor() -> UIColor{
        return theme.layoutBackgroundColor
    }
    
    static func cellBackgroundColor() -> UIColor{
        //return theme.cellBackgroundColor
        return theme.navigationBarBackgroundColor
    }
    
    static func generateColor(_ red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
}
