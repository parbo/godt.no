//
//  MessageView.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 20/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import UIKit

class MessageView : UIView{
    
    weak var label : UILabel?
    
    static func addMessageView(text: String,view: UIView){
        
        let messageView = MessageView()
        messageView.backgroundColor = UIColor.clear
        view.addSubview(messageView)
        messageView.frame = view.bounds
        messageView.isUserInteractionEnabled = false 
        messageView.snp.makeConstraints { (make) in
            make.width.equalTo(view).offset(-40)
            make.height.equalTo(200)
            make.center.equalTo(view)
        }
        
        let label = UILabel()
        label.text = text
        label.backgroundColor = UIColor.clear
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.lightGray
        label.numberOfLines = 3
        messageView.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.width.height.equalTo(messageView)
            make.center.equalTo(messageView)
        }
    }
}
