//
//  Messages.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 19/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation

let ERROR_LOADING_RECIPES = "Couldn't load recipes"
