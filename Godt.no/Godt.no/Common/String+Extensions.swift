//
//  String+Attributed.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 21/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import UIKit

extension String{
    
    func toAttributedHtml(color: UIColor, fontSize: Float) -> NSAttributedString?{
        if let temp = try? NSMutableAttributedString(
            data: (self.data(using: String.Encoding.unicode))!,
            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                      NSForegroundColorAttributeName: color],
            documentAttributes: nil){
            temp.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSMakeRange(0, temp.length))
            temp.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: CGFloat(fontSize)), range: NSMakeRange(0, temp.length))
            return temp
        }else{
            return nil
        }
        
    }
    
    func toAttributedHtml(color: UIColor) -> NSAttributedString?{
        
        return toAttributedHtml(color: color, fontSize: 13)                
    }
    
    func escapeHtmlTags(replacement: String) -> String{
        let res =  self.replacingOccurrences(of: "<[^>]+>", with: replacement, options: .regularExpression)
        return res
    }
}
