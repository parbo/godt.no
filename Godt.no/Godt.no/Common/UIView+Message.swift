//
//  UIView+Message.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 20/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    func showMessage(text: String){
        MessageView.addMessageView(text: text, view: self)
    }
    
    func removeMessage(){
        
        let subviews = self.subviews
        var messageView : MessageView?
        for item in subviews{
            if item.isKind(of: MessageView.self){
                messageView = item as? MessageView
                break
            }
        }
        messageView?.removeFromSuperview()
    }
}
