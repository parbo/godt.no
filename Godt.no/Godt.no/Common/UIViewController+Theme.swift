//
//  UIViewController+Theme.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 21/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{

    public func applyTheme(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.barTintColor = UIColor.navigationBarTintColor()
        
        self.view.backgroundColor = UIColor.navigationBarBackgroundColor()
    }
}
