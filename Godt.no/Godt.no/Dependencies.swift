//
//  Dependencies.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 18/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation

class Dependencies{
    
    func configureRecipesList(_ recipesViewController: RecipesListViewController){
        
        let interactor = RecipesListInteractor()
        interactor.dbProvider = RecipesDbProvider()
        interactor.networkProvider = RecipesNetworkProvider()
        let presenter = RecipesListPresenter()
        presenter.interactor = interactor
        presenter.view = recipesViewController
        recipesViewController.presenter = presenter
        
    }
}
