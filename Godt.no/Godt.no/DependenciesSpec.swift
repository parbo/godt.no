//
//  DependenciesSpec.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 18/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Godt_no

class DependenciesSpec: QuickSpec{
    
    override func spec() {
        
        describe("Dependencies"){
            
            var sut : Dependencies!
            
            beforeEach {
                sut = Dependencies()
            }
            
            describe("Should configure recipes list"){
            
                var recipesViewController: RecipesListViewController!
                
                beforeEach {
                    recipesViewController = RecipesListViewController()
                    sut.configureRecipesList(recipesViewController)
                }
                
                it(""){
                    expect(recipesViewController.presenter).toNot(beNil())
                    expect(recipesViewController.presenter.view).toNot(beNil())
                    expect(recipesViewController.presenter.interactor).toNot(beNil())
                    expect(recipesViewController.presenter.interactor.dbProvider).toNot(beNil())
                    expect(recipesViewController.presenter.interactor.networkProvider).toNot(beNil())
                }
            }
        }
    }
}
