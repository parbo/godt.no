//
//  Image.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 19/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Image : Object,Mappable{

    dynamic var imboId : String?
    dynamic var url : String?
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        imboId <- map["imboId"]
        url <- map["url"]
    }
}
