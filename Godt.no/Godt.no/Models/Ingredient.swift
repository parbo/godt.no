//
//  Ingredient.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 18/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Ingredient : Object,Mappable{
    
    dynamic var id : String?
    dynamic var name : String?
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }

}
