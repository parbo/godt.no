//
//  Recipe+Filtering.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 21/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation

extension Recipe{
    
    func containsAllPhrasesInTitleOrIngredients(phrases : [String]) -> Bool{
        
        if self.title == nil{
            return false
        }
        
        let filtered = phrases.filter { (phrase) -> Bool in
            let lowercasedPhrase = phrase.lowercased()
            return (self.title?.lowercased().contains(lowercasedPhrase))! || self.ingredients.filter{ ($0.name != nil) && ($0.name!.lowercased().contains(lowercasedPhrase)) }.count > 0
        }
        
        return filtered.count == phrases.count
        //return filtered.count > 0
    }
}
