//
//  Recipe.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 18/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift


class Recipe : Object,Mappable{
    
    dynamic var id : Int = 0;
    dynamic var title : String?
    dynamic var recipeDescription : String?
    
    let ingredients = List<Ingredient>();
    let steps = List<Step>();
    let images = List<Image>();
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        recipeDescription <- map["description"]
        ingredients <- map["ingredients"]
        steps <- map["steps"]
        images <- map["images"]
    }
}
