//
//  Step.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 19/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation

import ObjectMapper
import RealmSwift

class Step : Object,Mappable{
    
    dynamic var id : String?
    dynamic var stepDescription : String?
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        stepDescription <- map["description"]
    }
}
