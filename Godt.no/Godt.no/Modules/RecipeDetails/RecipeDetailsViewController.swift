//
//  RecipeDetailsViewController.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 21/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import TTTAttributedLabel

class RecipeDetailsViewController : UIViewController{
 
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var descriptionLabel : TTTAttributedLabel!
    @IBOutlet weak var ingredientsLabel : UILabel!
    
    var recipe : Recipe!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.applyTheme()
        self.addImage()
        self.addTitle()
        self.addDescription()
        self.addIngredients()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        var contentViewSize = self.contentView.frame.size
        let lastSubview = contentView.subviews.last!
        contentViewSize.height = lastSubview.frame.origin.y + lastSubview.frame.size.height + 10
        self.contentView.frame.size = contentViewSize
        self.scrollView.contentSize = self.contentView.frame.size
    }
    
    private func addImage(){
        if recipe.images.count > 0{
            let image = recipe.images[0]
            if (image.url != nil){
                self.imageView?.kf.setImage(with: URL(string: image.url!))
                self.imageView?.layer.cornerRadius = 8
                self.imageView?.clipsToBounds = true
            }
        }
    }
    
    private func addTitle(){
        self.titleLabel.textColor = UIColor.orange
        self.titleLabel.text = recipe.title
    }
    
    private func addDescription(){
        self.descriptionLabel.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue;
        self.descriptionLabel.delegate = self;
        if let attributeddesc = recipe.recipeDescription?.toAttributedHtml(color: UIColor.white){
            self.descriptionLabel.attributedText = attributeddesc
        }else{
            self.descriptionLabel.text = recipe.recipeDescription
        }
        
    }
    
    private func addIngredients(){
        
        self.ingredientsLabel.textColor = UIColor.lightGray
        self.ingredientsLabel.font = UIFont.boldSystemFont(ofSize: 16)
        
        var referenceView : UILabel = self.ingredientsLabel
        
        for ingredient in self.recipe!.ingredients{
            if ingredient.name != nil && (ingredient.name?.length)! > 0{
                let name = ingredient.name!
                let label = UILabel()
                label.text = "◦ " + name
                label.textColor = self.ingredientsLabel.textColor
                label.font = UIFont.systemFont(ofSize: 14)
                self.contentView.addSubview(label)
                label.snp.makeConstraints({ (make) in
                    make.left.equalTo(self.imageView).offset(8)
                    make.top.equalTo(referenceView).offset(30)
                })
                referenceView = label
            }
        }
    }
    
    @IBAction func doneButtonClicked(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    deinit{
        logDebug("Details")
    }
 
    
}

extension RecipeDetailsViewController : TTTAttributedLabelDelegate{
    
    @objc(attributedLabel:didSelectLinkWithURL:) func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        logDebug("")
    }
}
