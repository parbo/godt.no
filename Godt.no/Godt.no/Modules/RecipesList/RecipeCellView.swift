//
//  RecipeCellView.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 19/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import UIKit
import DisplaySwitcher
import SnapKit


class RecipeCellView : UICollectionViewCell{
    
    static let Identifier = "RecipeCellView"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var gridTitleContainer : UIView!
    @IBOutlet weak var gridTitleLabel : UILabel!
    
    private var imageWidthConstaint : Constraint?
    
    func updateFor(newLayoutState: LayoutState,animationDuration: TimeInterval){
        
        if newLayoutState == .list{
            //self.titleLabel.isHidden = false
            //self.descriptionLabel.isHidden = false
            self.gridTitleContainer.alpha = 0;
        }else{
            //self.titleLabel.isHidden = true
            //self.descriptionLabel.isHidden = true
            self.gridTitleContainer.alpha = 1
        }        
    }
    
    override func prepareForReuse() {
        self.descriptionLabel.text = nil
        self.descriptionLabel.backgroundColor = UIColor.cellBackgroundColor()
        self.gridTitleLabel.text = nil
        self.titleLabel.text = nil
        self.titleLabel.backgroundColor = UIColor.cellBackgroundColor()
        self.imageView.image = nil        
    }
    
    func prepareForRecycle(){
        self.imageView.image = nil
    }
    
    
}
