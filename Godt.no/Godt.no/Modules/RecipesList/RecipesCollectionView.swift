//
//  RecipesCollectionView.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 20/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import UIKit

class RecipesCollectionView : UICollectionView{
    
    var lastCellFrame : CGRect?
    
    override func startInteractiveTransition(to layout: UICollectionViewLayout, completion: UICollectionViewLayoutInteractiveTransitionCompletion? = nil) -> UICollectionViewTransitionLayout {
        lastCellFrame = self.subviews.last?.frame
        return super.startInteractiveTransition(to: layout, completion: completion)
    }
    
    override func reloadData() {
        super.reloadData()        
        
    }
}
