//
//  RecipesListInteractor.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 19/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation

class RecipesListInteractor{
    
    var networkProvider : RecipesNetworkProvider!
    var dbProvider : RecipesDbProvider!
    
    func loadRecipes(onSuccess successCallback: @escaping ([Recipe]?) -> (),
                     onFailure failureCallback: @escaping (String?) -> ()){
        
        dbProvider.loadRecipes { (recipes) in
            if recipes != nil && (recipes?.count)! > 0{
               successCallback(recipes)
            }else{
                self.networkProvider.loadRecipes(
                    for: UrlFactory.recipesUrl(),
                    onSuccess: { (recipes) in
                        logDebug("success")
                        successCallback(recipes)
                        if recipes != nil{
                            self.dbProvider.saveRecipes(recipes!)
                        }
                    },
                    onFailure: { (error) in
                        logDebug("failure")
                        failureCallback(error)
                })
            }
        }
                
    }
    
    

}
