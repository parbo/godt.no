//
//  RecipesListInteractorSpec.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 23.10.2016.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Godt_no

class RecipesListInteractorSpec: QuickSpec{
    
    override func spec() {
        
        describe("Recipes list interactor"){
            
            var sut: RecipesListInteractor!
            var networkProvider : RecipesNetworkProviderFake!
            var dbProvider : RecipesDbProviderFake!
            
            beforeEach {
                sut = RecipesListInteractor()
                dbProvider = RecipesDbProviderFake()
                sut.dbProvider = dbProvider
                networkProvider = RecipesNetworkProviderFake()
                sut.networkProvider = networkProvider
            }
            
            describe("when loading recipes"){
                
                describe("and no items in db"){
                    
                    beforeEach {
                        networkProvider.recipes = getTestRecipes()
                        sut.loadRecipes(onSuccess: { (recipes) in }, onFailure: { (error) in })
                    }
                
                    it("should call db provider"){
                        expect(dbProvider.loadCalled).to(beTrue())
                    }
                    
                    it("should call network provider"){
                        expect(networkProvider.loadCalled).to(beTrue())
                    }
                    
                    it("should load data from the network and save to db"){
                        expect(dbProvider.saveCalled).to(beTrue())
                    }
                }
                
                describe("and items are in db"){
                    
                    beforeEach {
                        dbProvider.recipes = getTestRecipes()
                        sut.loadRecipes(onSuccess: { (recipes) in }, onFailure: { (error) in })
                    }
                    
                    it("should call db provider"){
                        expect(dbProvider.loadCalled).to(beTrue())
                    }
                    
                    it("should call network provider"){
                        expect(networkProvider.loadCalled).to(beFalse())
                    }                    
                }
            }
        }
    }
}

fileprivate class RecipesNetworkProviderFake : RecipesNetworkProvider{
    
    var recipes : [Recipe]?
    
    var loadCalled  = false
    
    override func loadRecipes(for url: String, onSuccess successCallback: (@escaping ([Recipe]?) -> ()), onFailure failureCallback: (@escaping (String?) -> ())) {
        self.loadCalled = true;
        successCallback(recipes)
    }
}

fileprivate class RecipesDbProviderFake : RecipesDbProvider{
    
    var recipes : [Recipe]?
    
    var loadCalled  = false
    var saveCalled = false
    
    override func loadRecipes(completion: @escaping ([Recipe]?) -> ()) {
        self.loadCalled = true;
        completion(recipes)
    }
    
    override func saveRecipes(_ recipes: [Recipe]) {
        self.saveCalled = true
    }
}
