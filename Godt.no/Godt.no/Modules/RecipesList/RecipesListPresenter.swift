//
//  RecipesListPresenter.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 18/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation

open class RecipesListPresenter{

    var interactor : RecipesListInteractor!
    weak var view : RecipesListView!
    
    fileprivate var recipes : [Recipe]?
    
    func updateView(){
                
        view.prepareForUpdate()
        view.showLoadingScreen()
        
        interactor.loadRecipes(
            onSuccess:{ (recipes) in
                self.recipes = recipes
                self.view.hideLoadingScreen()
                self.view.displayRecipes(recipes)
            },
            onFailure: { (error) in
                self.view.hideLoadingScreen()
                if error != nil{
                    self.view.showError(error!)
                }else{
                    self.view.showError(ERROR_LOADING_RECIPES)
                }
        })
        
    }
    
    func filterRecipes(for text: String){
        
        let text = text.lowercased()
        
        if recipes == nil{
            return
        }
        
        if text.length == 0{
            self.view.displayRecipes(self.recipes)
            return
        }
        
        var filtered = [Recipe]()
        let commaSeparatedPhrases = text.components(separatedBy: ",").filter{$0.length > 0}
        for recipe in self.recipes!{
            if recipe.containsAllPhrasesInTitleOrIngredients(phrases: commaSeparatedPhrases){
                filtered.append(recipe)
            }
        }
        self.view.displayRecipes(filtered)
        
    }
    
}
