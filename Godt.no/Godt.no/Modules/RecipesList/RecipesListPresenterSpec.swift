//
//  RecipesListPresenterSpec.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 23.10.2016.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import Quick
import Nimble
import RealmSwift

@testable import Godt_no

class RecipesListPresenterSpec: QuickSpec{
    
    override func spec() {
        
        describe("Recipes List Presenter"){
            
            var sut: RecipesListPresenter!
            
            beforeEach{
                
                sut = RecipesListPresenter()
            }
            
            describe("should call"){
                
                var view: RecipesListViewFake!
                
                beforeEach {
                    view = RecipesListViewFake()
                    sut.view = view
                }
                
                describe("when updating view"){
                    
                    var interactor: RecipesListInteractorFake!
                    
                    beforeEach {
                        interactor = RecipesListInteractorFake()
                        sut.interactor = interactor
                    }
                    
                    describe("and when begining") {
                        beforeEach {
                            sut.updateView()
                        }
                        
                        it("prepareForUpdate()"){
                            expect(view.prepareCalled).to(beTrue())
                        }
                        
                        it("showLoadingScreen()"){
                            expect(view.showLoadingViewCalled).to(beTrue())
                        }
                    }
                    
                    describe("and when error occured", {
                        
                        beforeEach {
                            interactor.error = "Error"
                            sut.updateView()
                        }
                        
                        it("hideLoadingScreen()"){
                            expect(view.hideLoadingViewCalled).to(beTrue())
                        }
                        
                        it("showError()"){
                            expect(view.showErrorCalled).to(beTrue())
                        }
                    })
                    
                    describe("and when response is empty", {
                        
                        beforeEach {
                            interactor.recipes = [Recipe]()
                            sut.updateView()
                        }
                        
                        it("hideLoadingScreen()"){
                            expect(view.hideLoadingViewCalled).to(beTrue())
                        }
                        
                        it("displayRecipes()"){
                            expect(view.displayRecipesCalled).to(beTrue())
                        }
                    })
                    
                    describe("and when response is not empty", {
                        
                        beforeEach {
                            interactor.recipes = getTestRecipes()
                            sut.updateView()
                        }
                        
                        it("hideLoadingScreen()"){
                            expect(view.hideLoadingViewCalled).to(beTrue())
                        }
                        
                        it("displayRecipes()"){
                            expect(view.displayRecipesCalled).to(beTrue())
                        }
                    })
                }
                
                describe(""){
                    
                    var interactor: RecipesListInteractorFake!
                    
                    beforeEach {
                        interactor = RecipesListInteractorFake()
                        interactor.recipes = getTestRecipes()
                        sut.interactor = interactor
                    }
                    
                    describe("and when searching") {
                        beforeEach {
                            sut.updateView()
                        }
                        
                        it("should recive one item for phrase Hummus,Topping"){
                            sut.filterRecipes(for: "Hummus,Topping")
                            expect(view.filteredRecipes?.count) == 1
                        }
                        
                        it("should recive two items for phrase Pensling"){
                            sut.filterRecipes(for: "Pensling")
                            expect(view.filteredRecipes?.count) == 2
                        }
                        
                        it("should recive two items for phrase Topping"){
                            sut.filterRecipes(for: "Topping")
                            expect(view.filteredRecipes?.count) == 3
                        }
                        
                        it("should recive no items for phrase FakeTestPhrase"){
                            sut.filterRecipes(for: "FakeTestPhrase")
                            expect(view.filteredRecipes?.count) == 0
                        }
                    }
                }
            }
        }
    }
}


fileprivate class RecipesListViewFake : RecipesListView{
    
    var prepareCalled : Bool = false
    var displayRecipesCalled : Bool = false
    var showErrorCalled : Bool = false
    var showLoadingViewCalled : Bool = false
    var hideLoadingViewCalled : Bool = false
    
    var filteredRecipes : [Recipe]?
    
    func prepareForUpdate(){
        self.prepareCalled = true
    }
    
    func displayRecipes(_ recipes: [Recipe]?){
        self.displayRecipesCalled = true
        self.filteredRecipes = recipes
    }
    
    func showError(_ errro: String){
        self.showErrorCalled = true
    }
    
    func showLoadingScreen(){
        self.showLoadingViewCalled = true
    }
    
    func hideLoadingScreen(){
        self.hideLoadingViewCalled = true
    }
}

fileprivate class RecipesListInteractorFake : RecipesListInteractor{
    
    var recipes : [Recipe]!
    var error : String!
    
    override func loadRecipes(onSuccess successCallback: @escaping ([Recipe]?) -> (), onFailure failureCallback: @escaping (String?) -> ()) {

        if error != nil{
            failureCallback(error)
        }else{
            successCallback(recipes)
        }
    }
}


