//
//  File.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 19/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation

protocol RecipesListView : class {
    
    func prepareForUpdate()
    
    func displayRecipes(_ recipes: [Recipe]?);
    
    func showError(_ errro: String);
    
    func showLoadingScreen();
    
    func hideLoadingScreen();
}
