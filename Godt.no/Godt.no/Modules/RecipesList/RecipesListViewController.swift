//
//  RecipesListViewController.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 18/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import UIKit
import DisplaySwitcher
import Kingfisher
import Spring
import PullToRefreshSwift
import BubbleTransition

private let animationDuration: TimeInterval = 0.3

class RecipesListViewController : UIViewController{
    
    var presenter : RecipesListPresenter!
    
    @IBOutlet fileprivate weak var collectionView : UICollectionView!
    @IBOutlet fileprivate weak var searchBar: UISearchBar!
    
    //MARK: Configure Layouts
    let margin = CGFloat(6)
    var listLayout : DisplaySwitchLayout?
    var gridLayout : DisplaySwitchLayout?
    
    // MARK: Transition
    var bubbleTransition: BubbleTransition?
    var transitionStartPoint : CGPoint?
    internal var currentIndexPath : IndexPath?
    
    //MARK: -
    internal var recipes : [Recipe]?
    internal var layoutState: LayoutState = .list
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.applyTheme()
        self.setupSearchBar()
        self.setupCollectionView()
        self.addRefreshControl()
        
        self.presenter.updateView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    fileprivate func setupSearchBar(){
        self.searchBar.barTintColor = UIColor.navigationBarTintColor()
        self.searchBar.tintColor = UIColor.navigationBarTintColor()
        self.searchBar.isUserInteractionEnabled = false
        self.searchBar.returnKeyType = .done
    }
    
    fileprivate func addRefreshControl(){
        self.collectionView?.addPullRefresh(refreshCompletion: { () in
            self.presenter.updateView()
            self.collectionView?.stopPullRefreshEver()
        })
    }
    
    private func setupCollectionView(){
        // setup layouts
        let listLayoutCellHeight = self.view.frame.size.height / 6
        let gridLayoutCellHeight = (self.view.frame.size.width - 2 * margin) / 3
        listLayout = DisplaySwitchLayout(staticCellHeight: listLayoutCellHeight,
                                                          nextLayoutStaticCellHeight: gridLayoutCellHeight,
                                                          layoutState: .list)      
        gridLayout = DisplaySwitchLayout(staticCellHeight: gridLayoutCellHeight,
                                                          nextLayoutStaticCellHeight: listLayoutCellHeight,
                                                          layoutState: .grid)
        //
        self.layoutState = .list
        self.collectionView.backgroundColor = UIColor.layoutBackgroundColor()
        self.collectionView.register(UINib(nibName: "RecipeCell",bundle: nil), forCellWithReuseIdentifier:  RecipeCellView.Identifier)
        self.collectionView.collectionViewLayout = listLayout!
        
        self.collectionView.alwaysBounceVertical = true
    }
    
    func switchButtonTapped(sender: UIButton){
    
        let transitionManager: DisplaySwitcher.TransitionManager
        if layoutState == .list {
            layoutState = .grid
            transitionManager = TransitionManager(duration: animationDuration, collectionView: collectionView!, destinationLayout: gridLayout!, layoutState: layoutState)
        } else {
            layoutState = .list
            transitionManager = TransitionManager(duration: animationDuration, collectionView: collectionView!, destinationLayout: listLayout!, layoutState: layoutState)
        }
        transitionManager.startInteractiveTransition()
        updateButton()
    }
    
    fileprivate func updateButton(){
        
        if layoutState == .list{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "grid"),style: .plain, target: self, action: #selector(RecipesListViewController.switchButtonTapped(sender:)))
        }else{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "list"),style: .plain, target: self, action: #selector(RecipesListViewController.switchButtonTapped(sender:)))
        }
    }
    
}

//MARK: CollectionView Delegate & Datasource
extension RecipesListViewController : UICollectionViewDelegate, UICollectionViewDataSource{

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let recipes = self.recipes{
            return recipes.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let recipe = self.recipes![indexPath.row]
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: RecipeCellView.Identifier, for: indexPath) as! RecipeCellView
        cell.prepareForReuse()
        
        if layoutState == .grid {
            cell.gridTitleLabel.text = recipe.title
        } else {
            if let escaped = recipe.recipeDescription?.escapeHtmlTags(replacement: ""){
                let max = 200
                if escaped.length > max{
                    let index = escaped.index(escaped.startIndex, offsetBy: max)
                    cell.descriptionLabel.text = escaped.substring(to: index)
                }else{
                    cell.descriptionLabel.text = escaped
                }
            }
        }
        cell.backgroundColor = UIColor.cellBackgroundColor()
        
        cell.titleLabel.text = recipe.title
      
        if recipe.images.count > 0{
            let image = recipe.images[0]
            if (image.url != nil){
                cell.imageView?.kf.setImage(with: URL(string: image.url!))
            }
        }
        
        cell.updateFor(newLayoutState: layoutState, animationDuration: animationDuration)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, transitionLayoutForOldLayout fromLayout: UICollectionViewLayout, newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
        let customTransitionLayout = TransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
        return customTransitionLayout
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        transitionStartPoint = cell?.center
        transitionStartPoint?.y = (transitionStartPoint?.y)! + collectionView.frame.origin.y - collectionView.contentOffset.y
        currentIndexPath = indexPath
        collectionView.deselectItem(at: indexPath, animated: false)
        if self.searchBar.isFirstResponder{
            self.searchBar.resignFirstResponder()
        }
        makeTransition()        
    }

}

//MARK: Presenter output
extension RecipesListViewController : RecipesListView{
    
    func prepareForUpdate() {
        self.recipes = nil
        self.view.removeMessage()
        self.collectionView?.reloadData()
        self.navigationItem.rightBarButtonItem = nil
        self.searchBar.isUserInteractionEnabled = false
    }
    
    func displayRecipes(_ recipes: [Recipe]?){
        logDebug("show recipes")
        self.recipes = recipes
        self.collectionView.reloadData()
        if recipes != nil && (recipes?.count)! > 0{
            self.updateButton()
            self.searchBar.isUserInteractionEnabled = true
        }
    }
    
    func showError(_ errro: String){
        logDebug("show error")
        self.view.showMessage(text: errro + " Pull down to refresh.")
    }
    
    func showLoadingScreen(){
        logDebug("show loading screen")
        self.view.showLoading()
    }
    
    func hideLoadingScreen(){
        logDebug("hide loading screen")
        self.view.hideLoading()
    }
    
}

//MARK: Search
extension RecipesListViewController : UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.setShowsCancelButton(true, animated: true)
        self.searchBar.barTintColor = UIColor.navigationBarTintColor()
        self.collectionView?.removePullRefresh()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.barTintColor = UIColor.navigationBarTintColor()
        self.addRefreshControl()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.resignFirstResponder()
        self.presenter.filterRecipes(for: "")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let text = searchBar.text{
            presenter.filterRecipes(for: text)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}


//MARK: Transition
extension RecipesListViewController : UIViewControllerTransitioningDelegate{
    
    fileprivate func makeTransition(){
        self.bubbleTransition = BubbleTransition()
        self.bubbleTransition!.duration = 0.5
        self.performSegue(withIdentifier: "BubbleTransition", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let navigationController = segue.destination as! UINavigationController
        navigationController.transitioningDelegate = self
        navigationController.modalPresentationStyle = .custom
        let detailsController = navigationController.topViewController as! RecipeDetailsViewController!
        detailsController?.recipe = self.recipes?[(currentIndexPath?.row)!]
    }        
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        bubbleTransition!.transitionMode = .present
        bubbleTransition!.startingPoint = transitionStartPoint!
        bubbleTransition!.bubbleColor = UIColor.navigationBarBackgroundColor()
        return bubbleTransition!
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        bubbleTransition!.transitionMode = .dismiss
        bubbleTransition!.startingPoint = transitionStartPoint!
        bubbleTransition!.bubbleColor = UIColor.navigationBarBackgroundColor()
        return bubbleTransition
    }

}
