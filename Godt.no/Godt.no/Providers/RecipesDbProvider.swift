//
//  RecipesDbProvider.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 18/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import RealmSwift

open class RecipesDbProvider{
    
    
    func saveRecipes(_ recipes: [Recipe]){
       
        let realm = try! Realm()
        try! realm.write {
            realm.add(recipes)
        }
    }
    
    func loadRecipes(completion: @escaping ([Recipe]?) -> ()){
     
            let realm = try! Realm()            
            let recipes = realm.objects(Recipe.self)     
            completion(recipes.map({$0}))
    }
}
