//
//  RecipesNetworkProvider.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 18/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

open class RecipesNetworkProvider{
    
    weak var request : Request?
    
    func loadRecipes(
        for url: String,
        onSuccess successCallback: @escaping (([Recipe]?) -> ()),
        onFailure failureCallback: @escaping ((String?) -> ()) ) {
        
        request?.cancel()        
        request =  Alamofire.request(url, method: .get)
            .responseArray { (response: DataResponse<[Recipe]>) in
                switch response.result{
                    case .success(let array):
                        successCallback(array)
                    case .failure(let error):
                        failureCallback(error.localizedDescription)
                }
        }
    }
    
    fileprivate func createRequest(for url : String) -> DataRequest{
        return Alamofire.request(url, method: .get)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
    }
}
