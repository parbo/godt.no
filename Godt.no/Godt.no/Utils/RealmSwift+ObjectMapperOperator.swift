//
//  RealmSwift+ObjectMapperOperator.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 19/10/16.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation

import Foundation
import RealmSwift
import ObjectMapper

// Swift 3
infix operator <-

/// Object of Realm's List type
public func <- <T: Mappable>(left: List<T>, right: Map) {
    var array: [T]?
    
    if right.mappingType == .toJSON {
        array = Array(left)
    }
    
    array <- right
    
    if right.mappingType == .fromJSON {
        if let theArray = array {
            left.append(objectsIn: theArray)
        }
    }
}

/// Object of Realm's RealmOptional type
public func <- <T>(left: RealmOptional<T>, right: Map) {
    var optional: T?
    
    if right.mappingType == .toJSON {
        optional = left.value
    }
    
    optional <- right
    
    if right.mappingType == .fromJSON {
        if let theOptional = optional {
            left.value = theOptional
        }
    }
}
