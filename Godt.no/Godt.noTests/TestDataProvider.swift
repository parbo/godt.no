//
//  TestDataProvider.swift
//  Godt.no
//
//  Created by Boguslaw Parol on 23.10.2016.
//  Copyright © 2016 Boguslaw Parol. All rights reserved.
//

import Foundation

@testable import Godt_no

func getTestRecipes() -> [Recipe]{
    
    var recipes = [Recipe]()
    recipes.append(Recipe())
    recipes[0].title = "Hummus med duftende krydret kjøtt og auberginer"
    recipes[0].ingredients.append(Ingredient())
    recipes[0].ingredients[0].name = "Hummus"
    recipes[0].ingredients.append(Ingredient())
    recipes[0].ingredients[1].name = "Topping"
    recipes.append(Recipe())
    recipes[1].title = "Hvetekake med eplefyll"
    recipes[1].ingredients.append(Ingredient())
    recipes[1].ingredients[0].name = "Topping"
    recipes[1].ingredients.append(Ingredient())
    recipes[1].ingredients[1].name = "Pensling"
    recipes.append(Recipe())
    recipes[2].title = "Trines Matbloggs hvetekrans med sukker og kanel Topping"
    recipes[2].ingredients.append(Ingredient())
    recipes[2].ingredients[0].name = "Til pensling"
    recipes[2].ingredients.append(Ingredient())
    recipes[2].ingredients[1].name = "Pynt"
    
    return recipes
}
